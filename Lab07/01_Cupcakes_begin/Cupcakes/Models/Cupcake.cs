﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cupcakes.Models
{
    public class Cupcake
    {
        [Key]
        public int CupcakeId { set; get; }
        [Required(ErrorMessage = "Please select a cupcake type")]
        [Display(Name = "Cupcake Type:")]
        public CupcakeType? CupcakeType { set; get; }
        [Required(ErrorMessage = "Please enter a cupcake description")]
        [Display(Name = "Description")]
        public string Description { set; get; }
        public bool GlutenFree { set; get; }
        [Range(1, 15)]
        [Required(ErrorMessage = "Please enter a cupcake price")]
        [DataType(DataType.Currency)]
        public double? Price { set; get; }
        [NotMapped]
        [Display(Name = "Cupcake Picture")]
        public IFormFile PhotoAvatar { set; get; }
        public string ImageName { set; get; }
        public byte[] PhotoFile { set; get; }
        public string ImageMimeType { set; get; }
        [Required(ErrorMessage = " Please select a bakery")]
        public int? BakeryId { set; get; }
        public virtual Bakery Bakery { set; get; }
    }
}
